/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Scanner;

public class CommandFactory {
    private final TeamManager teamManager;
    private final Scanner sc;

    public CommandFactory(TeamManager teamManager, Scanner sc) {
        this.teamManager = teamManager;
        this.sc = sc;
    }

    public Command createCommand(String commandType) {
        try {
            switch (commandType) {
                case "c" -> {return new CreateTeamCmd(sc, teamManager);}
                case "g" -> {return new SetCurrentTeamCmd(sc, teamManager);}
                case "a" -> {return new AddPlayerCmd(sc, teamManager);}
                case "m" -> {return new SetPlayerPositionCmd(sc, teamManager);}
                case "s" -> {return new ListCurrentTeamCmd(teamManager);}
                case "d" -> {return new DeletePlayerCmd(sc, teamManager);}
                case "p" -> {return new ListAllTeamCmd(teamManager);}
                case "t" -> {return new SetTeamNameCmd(sc, teamManager, teamManager.getCurrentTeam());}
                case "u" -> {return new UndoCmd(teamManager);}
                case "r" -> {return new RedoCmd(teamManager);}
                case "l" -> {return new ListUndoRedoCmd(teamManager);}
                case "x" -> {return new ExitCmd();}
                default -> {return null;}
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}
