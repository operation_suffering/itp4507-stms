/*Lau Ka Fai IT114105-2B 220494381*/
public class RedoCmd implements Command {

    private final TeamManager teamManager;

    public RedoCmd(TeamManager teamManager) {
        this.teamManager = teamManager;
    }

    @Override
    public String getString() {
        // This method is not implemented, so it returns null
        return null;
    }

    @Override
    public void execute() {
        // Perform the redo operation using the TeamManager
        teamManager.redo();
    }
}