/*Lau Ka Fai IT114105-2B 220494381*/
public class Player {
    private final String playerID;
    private final String name;
    private int position;

    public Player(String id, String name) {
        if (id == null || name == null) {
            throw new IllegalArgumentException("Player ID and name cannot be null.");
        }
        this.name = name;
        this.playerID = id;
    }

    public String getPlayerID() {
        return playerID;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        // Add validation if needed
        this.position = position;
    }

    public String getName() {
        return name;
    }

}
