/*Lau Ka Fai IT114105-2B 220494381*/

import java.util.Scanner;

public class SetPlayerPositionCmd implements Command, Memento {
    private final Scanner sc;
    private final TeamManager teamManager;
    private String playerId;
    private int newPosition;
    private int oldPosition;
    private Team currentTeam;

    public SetPlayerPositionCmd(Scanner sc, TeamManager teamManager) {
        this.sc = sc;
        this.teamManager = teamManager;
    }

    // Executes the command, which updates the position of a player.
    @Override
    public void execute() {
        try {
            System.out.print("Please input player ID: ");
            playerId = sc.nextLine();
            currentTeam = teamManager.getCurrentTeam();
            int maxPosition = 0;
            if (currentTeam != null) {
                Player player = teamManager.getPlayer(playerId);

                if (player != null) {
                    if (currentTeam.getClass().getName().equals("FootballTeam")) {
                        System.out.print("Position (1 = goal keeper | 2 = defender | 3 = midfielder | 4 = forward): ");
                        maxPosition = 4;
                    } else if (currentTeam.getClass().getName().equals("VolleyballTeam")) {
                        System.out.print("Position (1 = attacker | 2 = defender): ");
                        maxPosition = 2;
                    }

                    newPosition = sc.nextInt();
                    sc.nextLine();
                    if (newPosition > maxPosition || newPosition < 1) {
                        throw new IllegalArgumentException("Invalid position. Please enter a position within the valid range.");
                    } else {
                        // Save the player's old position
                        oldPosition = player.getPosition();

                        // Modify the position
                        teamManager.updatePlayerPosition(playerId, newPosition);
                        teamManager.pushCommand(this);
                        System.out.println("Position is updated.");
                    }
                } else {
                    System.out.println("Player with ID " + playerId + " not found.");
                }
            } else {
                System.out.println("No current team set.");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


     // Undoes the previous update player position command's execution.
    @Override
    public void undo() {
        try {
            if (currentTeam != null) {
                Player player = teamManager.getPlayer(playerId);
                if (player != null) {
                    teamManager.updatePlayerPosition(playerId, oldPosition);
                    System.out.println("Undone: Player position updated to " + getPositionDescription(oldPosition));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    // Redoes the previously undone update player position command's execution.
    @Override
    public void redo() {
        try {
            teamManager.updatePlayerPosition(playerId, newPosition);
            System.out.println("Redone: Player position updated to " + getPositionDescription(newPosition));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


     // Gets the description of a player's position based on the current team's type.
    private String getPositionDescription(int position) {
        String[] footballPositions = {"goal keeper", "defender", "midfielder", "forward"};
        String[] volleyballPositions = {"attacker", "defender"};

        if (currentTeam.getClass().getName().equals("FootballTeam")) {
            return footballPositions[position - 1];
        } else if (currentTeam.getClass().getName().equals("VolleyballTeam")) {
            return volleyballPositions[position - 1];
        } else {
            return "unknown position";
        }
    }


     // Gets the string representation of the command.
    @Override
    public String getString() {
        //eg. Modify player’s position, p001, defender
        return "Modify player's position, " + playerId + ", " + getPositionDescription(newPosition);
    }
}