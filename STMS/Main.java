/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Scanner;

public class Main {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Create TeamManager to manage teams
        TeamManager teamManager = new TeamManager();

        // Create CommandFactory to create commands
        CommandFactory commandFactory = new CommandFactory(teamManager, sc);

        // Get input in a while-loop until the user chooses to exit
        while (true) {
            System.out.println("Sport Teams Management System (STMS)");
            System.out.printf("c = create team, g = set current team, a = add player, m = modify player's position,%n"
                    + "d = delete player, s = show team, p = display all teams, t = change team's name,%n"
                    + "u = undo, r = redo, l = list undo/redo, x = exit system%n");

            // Check if the current team is set; if yes, print current team
            if (teamManager.checkTeamList()) {
                System.out.println(teamManager.showCurrentTeam());
            }

            System.out.print("Please enter command [ c | g | a | m | d | s | p | t | u | r | l | x ] :-");
            String commandType = sc.nextLine();

            // Create the command based on user input
            Command command = commandFactory.createCommand(commandType);

            // Execute the command if it's not null
            if (command != null) {
                command.execute();
            } else {
                System.out.println("Invalid command. Please try again.");
            }
        }
    }
}