/*Lau Ka Fai IT114105-2B 220494381*/
 class UndoCmd implements Command, Memento {
    private final TeamManager teamManager;

    public UndoCmd(TeamManager teamManager) {
        this.teamManager = teamManager;
    }

    @Override
    public String getString() {
        return null;
    }

    @Override
    public void execute() {
        teamManager.undo();
    }

    @Override
    public void undo() {}

    @Override
    public void redo() {}

}
