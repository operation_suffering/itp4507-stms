/*Lau Ka Fai IT114105-2B 220494381*/

import java.util.Enumeration;
import java.util.Vector;

public abstract class Team {
    private final String teamId;
    private final Vector<Player> players;
    private String name;

    // Constructs a Team object with the specified team ID.
    public Team(String teamID) {
        players = new Vector<>();
        this.teamId = teamID;
    }

    public String getTeamID() {
        return teamId;
    }

    public String getName() {
        return name;
    }

    // Sets the name of the team.
    public void setName(String name) {
        this.name = name;
    }

    // Adds a player to the team.
    public void addPlayer(Player player) {
        players.add(player);
    }

    // Removes a player from the team.
    public void removePlayer(Player player) {
        players.remove(player);
    }

    // Returns an enumeration of all players in the team.
    public Enumeration<Player> getAllPlayers() {
        return players.elements();
    }

    // Displays information about the team.
    public abstract void displayTeam();
}