/*Lau Ka Fai IT114105-2B 220494381*/
public class ListAllTeamCmd implements Command, Memento {
    private final TeamManager teamManager;

    public ListAllTeamCmd(TeamManager teamManager) {
        this.teamManager = teamManager;
    }

    @Override
    public void execute() {
        // Call the teamManager's method to display all teams
        teamManager.displayAllTeams();
    }

    @Override
    public void undo() {
        // No action required for undo in this command
    }

    @Override
    public void redo() {
        // No action required for redo in this command
    }

    @Override
    public String getString() {
        // There is no meaningful string representation to return
        return null;
    }
}