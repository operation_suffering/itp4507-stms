/*Lau Ka Fai IT114105-2B 220494381*/
public class ListUndoRedoCmd implements Command {
    private final TeamManager teamManager;

    public ListUndoRedoCmd(TeamManager teamManager) {
        this.teamManager = teamManager;
    }

    @Override
    public void execute() {
        // Call the teamManager's methods to show the lists of undo and redo commands
        teamManager.showUndoList();
        teamManager.showRedoList();
    }

    @Override
    public String getString() {
        // There is no meaningful string representation to return
        return null;
    }
}