/*Lau Ka Fai IT114105-2B 220494381*/
public interface Memento {
    String getString();
    public void undo();
    public void redo();
}