/*Lau Ka Fai IT114105-2B 220494381*/
public interface Command {
    String getString();
    void execute();

}
