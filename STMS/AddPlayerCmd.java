/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Scanner;

public class AddPlayerCmd implements Command, Memento {
    private final Scanner sc;
    private final TeamManager teamManager;
    private Team currentTeam;
    private String playerID;
    private String playerName;
    private Player addedPlayer;
    private int position;

    public AddPlayerCmd(Scanner sc, TeamManager teamManager) {
        this.sc = sc;
        this.teamManager = teamManager;
    }

    @Override
    public void execute() {
        try {
            System.out.print("Please input player information (id, name):");
            String input = sc.nextLine();
            String[] strArr = input.split(", ");
            playerID = strArr[0];
            playerName = strArr[1];

            currentTeam = teamManager.getCurrentTeam();
            if (currentTeam != null) {
                int maxPosition = 0;
                if (currentTeam instanceof FootballTeam) {
                    System.out.print("Position (1 = goal keeper | 2 = defender | 3 = midfielder | 4 = forward): ");
                    maxPosition = 4;
                } else if (currentTeam instanceof VolleyballTeam) {
                    System.out.print("Position (1 = attacker | 2 = defender): ");
                    maxPosition = 2;
                }
                position = sc.nextInt();
                sc.nextLine();
                if (position > maxPosition || position < 1) {
                    throw new IllegalArgumentException("Invalid position. Please enter a position within the valid range.");
                } else {
                    addedPlayer = PlayerFactory.createPlayer(playerID, playerName);
                    addedPlayer.setPosition(position);

                    currentTeam.addPlayer(addedPlayer);
                    System.out.println("Player is added.");
                }
            } else {
                System.out.println("No current team. Can't add player.");
            }
            teamManager.pushCommand(this);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void undo() {
        if (currentTeam != null) {
            currentTeam.removePlayer(addedPlayer);
        }
    }

    @Override
    public void redo() {
        if (currentTeam != null) {
            addedPlayer = PlayerFactory.createPlayer(playerID, playerName);
            addedPlayer.setPosition(position);
            currentTeam.addPlayer(addedPlayer);
        }
    }

    private String getPositionDescription() {
        if (currentTeam instanceof FootballTeam) {
            String[] footballPositions = {"goal keeper", "defender", "midfielder", "forward"};
            return footballPositions[position - 1];
        } else if (currentTeam instanceof VolleyballTeam) {
            String[] volleyballPositions = {"attacker", "defender"};
            return volleyballPositions[position - 1];
        } else {
            return "unknown position";
        }
    }

    @Override
    public String getString() {
        return "Add player, " + playerID + ", " + playerName + ", " + getPositionDescription();
    }
}