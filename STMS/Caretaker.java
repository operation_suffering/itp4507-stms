/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Stack;

public class Caretaker {
    // Stack to store undo commands
    private final Stack<Memento> undoStack = new Stack<>();
    // Stack to store redo commands
    private final Stack<Memento> redoStack = new Stack<>();
    // Stack to store history of executed undo commands
    private final Stack<Memento> historyUndoList = new Stack<>();
    // Stack to store history of executed redo commands
    private final Stack<Memento> historyRedoList = new Stack<>();

    // Pushes a command to the undo stack and clears the redo stack
    public void pushCommand(Memento command) {
        undoStack.push(command);
        redoStack.clear();
    }

    // Performs an undo operation
    public void undo() {
        if (!undoStack.isEmpty()) {
            Memento command = undoStack.pop();
            command.undo();
            historyUndoList.push(command);
            redoStack.push(command);
        }
    }

    // Performs a redo operation
    public void redo() {
        if (!redoStack.isEmpty()) {
            Memento command = redoStack.pop();
            command.redo();
            historyRedoList.push(command);
            undoStack.push(command);
        }
    }

    // Displays the list of commands in the undo stack
    public void showUndoList() {
        System.out.println("Undo List");
        for (Memento command : undoStack) {
            System.out.println(command.getString());
        }
        System.out.println("-- End of undo list --");
    }

    // Displays the list of commands in the redo stack
    public void showRedoList() {
        System.out.println("Redo List");
        for (Memento command : redoStack) {
            System.out.println(command.getString());
        }
        System.out.println("-- End of redo list --");
    }

}