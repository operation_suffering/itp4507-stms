/*Lau Ka Fai IT114105-2B 220494381*/
public class ListCurrentTeamCmd implements Command, Memento {
    private final TeamManager teamManager;

    public ListCurrentTeamCmd(TeamManager teamManager) {
        this.teamManager = teamManager;
    }

    @Override
    public void execute() {
        // Get the current team from the team manager
        Team currentTeam = teamManager.getCurrentTeam();
        if (currentTeam != null) {
            // Display the current team
            currentTeam.displayTeam();
        } else {
            System.out.println("No current team to display.");
        }
    }

    @Override
    public void undo() {
        // No undo operation for displaying the current team
    }

    @Override
    public void redo() {
        // No redo operation for displaying the current team
    }

    @Override
    public String getString() {
        // There is no meaningful string representation to return
        return null;
    }
}