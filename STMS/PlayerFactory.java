/*Lau Ka Fai IT114105-2B 220494381*/
public class PlayerFactory {
    // Private constructor to prevent instantiation of the factory
    private PlayerFactory() {
        throw new AssertionError("PlayerFactory should not be instantiated.");
    }


    // Creates a new Player object with the specified ID and name.
    public static Player createPlayer(String id, String name) {
        return new Player(id, name);
    }

}