/*Lau Ka Fai IT114105-2B 220494381*/
public class TeamFactory {

    // Creates a new Team object based on the team ID and team type.
    public static Team createTeam(String teamID, String teamType) {

        if (teamType.equalsIgnoreCase("v")) {
            return new VolleyballTeam(teamID);
        } else if (teamType.equalsIgnoreCase("f")) {
            return new FootballTeam(teamID);
        } else {
            return null;
        }
    }

}