/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.*;

public class TeamManager {
    private final List<Team> teams;
    private Team currentTeam;
    private final Caretaker caretaker = new Caretaker();
    private final HashMap<String, Integer> oldPositions;

    public TeamManager() {
        this.teams = new ArrayList<>();
        oldPositions = new HashMap<>();
    }

    public boolean checkTeamList() {
        return teams != null && teams.contains(currentTeam);
    }

    public void addTeam(Team team) {
        this.teams.add(team);
        setCurrentTeam(team.getTeamID());
    }

    public void setCurrentTeam(String teamID) {
        for (Team team : this.teams) {
            if (team.getTeamID().equals(teamID)) {
                this.currentTeam = team;
                break;
            }
        }
    }

    public Team getCurrentTeam() {
        return this.currentTeam;
    }

    public void updatePlayerPosition(String playerId, int newPosition) {
        Player player = getPlayer(playerId);
        if (player != null) {
            // Save the old position
            int oldPosition = player.getPosition();
            oldPositions.put(playerId, oldPosition);

            // Modify the position
            player.setPosition(newPosition);
        }
    }

    public void deletePlayer(String playerId) {
        if (currentTeam != null) {
            Enumeration<Player> players = currentTeam.getAllPlayers();
            while (players.hasMoreElements()) {
                Player player = players.nextElement();
                if (player.getPlayerID().equals(playerId)) {
                    currentTeam.removePlayer(player);
                    break;
                }
            }
        } else {
            System.out.println("No team available");
        }
    }

    public void displayAllTeams() {
        for (Team team : teams) {
            System.out.println(team.getClass().getSimpleName() + " " + team.getName() + " (" + team.getTeamID() + ")");
        }
    }

    public void changeCurrentTeamName(String newName) {
        if (currentTeam != null) {
            currentTeam.setName(newName);
        } else {
            System.out.println("No current team set");
        }
    }

    public void undo() {
        caretaker.undo();
    }

    public void redo() {
        caretaker.redo();
    }

    public void showUndoList() {
        caretaker.showUndoList();
    }

    public void showRedoList() {
        caretaker.showRedoList();
    }

    public void removeTeam(Team team) {
        teams.remove(team);
        if (team.equals(currentTeam)) {
            if (teams.isEmpty()) {
                currentTeam = null;
            } else {
                currentTeam = teams.get(0);
            }
        }
    }

    public Player getPlayer(String playerID) {
        for (Team team : teams) {
            Enumeration<Player> players = team.getAllPlayers();
            while (players.hasMoreElements()) {
                Player player = players.nextElement();
                if (player.getPlayerID().equals(playerID)) {
                    return player;
                }
            }
        }
        return null;
    }

    public void addPlayer(Player player, String teamID) {
        for (Team team : teams) {
            if (team.getTeamID().equals(teamID)) {
                team.addPlayer(player);
                break;
            }
        }
    }

    public void pushCommand(Memento command) {
        caretaker.pushCommand(command);
    }

    public String showCurrentTeam() {
        return "The current team is " + currentTeam.getTeamID() + " " + currentTeam.getName();
    }

    public boolean teamExists(String teamID) {
        for (Team team : teams) {
            if (team.getTeamID().equals(teamID)) {
                return true;
            }
        }
        return false;
    }
}