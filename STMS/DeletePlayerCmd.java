/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Scanner;

public class DeletePlayerCmd implements Command, Memento {
    private final Scanner sc;
    private final TeamManager teamManager;
    private String playerID;
    private Player deletedPlayer;

    public DeletePlayerCmd(Scanner sc, TeamManager teamManager) {
        this.sc = sc;
        this.teamManager = teamManager;
    }

    @Override
    public void execute() {
        try {
            System.out.print("Please input player ID: ");
            playerID = sc.nextLine();

            // Save the player before deleting
            deletedPlayer = teamManager.getPlayer(playerID);

            if (deletedPlayer != null) {
                teamManager.deletePlayer(playerID);
            }
            teamManager.pushCommand(this);
            System.out.println("Player is deleted.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void undo() {
        if (deletedPlayer != null) {
            // Undo the deletion by adding the player back to the team
            teamManager.addPlayer(deletedPlayer, teamManager.getCurrentTeam().getTeamID());
        }
    }

    @Override
    public void redo() {
        if (deletedPlayer != null) {
            // Redo the deletion by deleting the player again
            teamManager.deletePlayer(playerID);
        }
    }

    @Override
    public String getString() {
        //e.g. Delete player, p001
        return "Delete player, " + playerID;
    }
}