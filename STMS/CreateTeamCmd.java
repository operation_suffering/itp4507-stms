/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Scanner;

public class CreateTeamCmd implements Command, Memento {
    private final Scanner sc;
    private final TeamManager teamManager;
    private String teamID;
    private String teamName;
    private Team newTeam;

    public CreateTeamCmd(Scanner sc, TeamManager teamManager) {
        this.sc = sc;
        this.teamManager = teamManager;
    }

    @Override
    public void execute() {
        try {
            System.out.print("Enter sport type (v = volleyball | f = football) :-");
            String type = sc.nextLine();

            System.out.print("Enter team ID :-");
            teamID = sc.nextLine();

            System.out.print("Enter team name :-");
            teamName = sc.nextLine();

            // Create a new team based on the provided type
            newTeam = TeamFactory.createTeam(teamID, type);
            newTeam.setName(teamName);
            teamManager.addTeam(newTeam);

            // Set the newly created team as the current team
            teamManager.setCurrentTeam(teamID);

            // Push this command to the command stack for undo/redo functionality
            teamManager.pushCommand(this);

            // Display success message and the current team's information
            Team currentTeam = teamManager.getCurrentTeam();
            System.out.println(currentTeam.getClass().getName() + " team is created.");
            System.out.println("Current team is changed to " + currentTeam.getTeamID());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void undo() {
        // Remove the newly created team and reset the current team
        teamManager.removeTeam(newTeam);
        teamManager.setCurrentTeam(null);
    }

    @Override
    public void redo() {
        // Add the previously removed team and set it as the current team
        teamManager.addTeam(newTeam);
        teamManager.setCurrentTeam(teamID);
    }

    @Override
    public String getString() {
        // Return a string representation of the command for recording purposes
        //e.g. Create football team, T102, KCF
        return "Create football team, " + teamID + ", " + teamName;
    }
}