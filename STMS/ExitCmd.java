/*Lau Ka Fai IT114105-2B 220494381*/
public class ExitCmd implements Command {

    @Override
    public void execute() {
        // Call System.exit() to terminate the program with a status code of 0
        System.exit(0);
    }

    @Override
    public String getString() {
        // There is no meaningful string representation to return
        return null;
    }
}