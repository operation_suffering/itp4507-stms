/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.Scanner;

public class SetTeamNameCmd implements Command, Memento {
    private final TeamManager teamManager;
    private final Scanner sc;
    private String newTeamName;
    private String oldTeamName;
    private final Team team;

    public SetTeamNameCmd(Scanner sc, TeamManager teamManager, Team team) {
        this.sc = sc;
        this.teamManager = teamManager;
        this.team = team;
    }

    @Override
    public void execute() {
        try {
            System.out.print("Please input new name of the current team: ");
            oldTeamName = teamManager.getCurrentTeam().getName();
            newTeamName = sc.nextLine();
            teamManager.changeCurrentTeamName(newTeamName);
            teamManager.pushCommand(this);
            System.out.println("Team's name is updated.");
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void undo() {
        System.out.println("Undoing: changing team name from " + newTeamName + " to " + oldTeamName);
        teamManager.changeCurrentTeamName(oldTeamName);
    }

    @Override
    public void redo() {
        System.out.println("Redoing: changing team name from " + oldTeamName + " to " + newTeamName);
        teamManager.changeCurrentTeamName(newTeamName);
    }

    @Override
    public String getString() {
        //e.g. Change team’s name, T001, STV
        return "Change team's name: " + team.getTeamID() + ", " + newTeamName;
    }
}