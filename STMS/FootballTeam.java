/*Lau Ka Fai IT114105-2B 220494381*/
import java.util.*;

public class FootballTeam extends Team {
    // Constants for different player positions
    private static final int GOALKEEPER_POSITION = 1;
    private static final int DEFENDER_POSITION = 2;
    private static final int MIDFIELDER_POSITION = 3;
    private static final int FORWARD_POSITION = 4;

    public FootballTeam(String teamID) {
        super(teamID);
    }

    public void displayTeam() {
        // StringBuilders to store players based on position
        StringBuilder goalkeeper = new StringBuilder();
        StringBuilder defenders = new StringBuilder();
        StringBuilder midfielder = new StringBuilder();
        StringBuilder forward = new StringBuilder();

        // Get all players in the team
        Enumeration<Player> players = getAllPlayers();
        while (players.hasMoreElements()) {
            Player player = players.nextElement();
            String playerInfo = player.getPlayerID() + ", " + player.getName() + "\n";
            // Categorize players based on their position
            if (player.getPosition() == GOALKEEPER_POSITION) {
                goalkeeper.append(playerInfo);
            } else if (player.getPosition() == DEFENDER_POSITION) {
                defenders.append(playerInfo);
            } else if (player.getPosition() == MIDFIELDER_POSITION) {
                midfielder.append(playerInfo);
            } else if (player.getPosition() == FORWARD_POSITION) {
                forward.append(playerInfo);
            }
        }

        // Display team information and players by position
        System.out.println(getName() + " (" + getTeamID() + ")");
        System.out.println("Goal Keeper:");
        if (goalkeeper.isEmpty()) {
            System.out.println("NIL");
        } else {
            System.out.print(goalkeeper);
        }
        System.out.println("Defender:");
        if (defenders.isEmpty()) {
            System.out.println("NIL");
        } else {
            System.out.print(defenders);
        }
        System.out.println("Midfielder:");
        if (midfielder.isEmpty()) {
            System.out.println("NIL");
        } else {
            System.out.print(midfielder);
        }
        System.out.println("Forward:");
        if (forward.isEmpty()) {
            System.out.println("NIL");
        } else {
            System.out.print(forward);
        }
    }
}